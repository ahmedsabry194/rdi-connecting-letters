﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterFiller : MonoBehaviour
{
    [SerializeField] private List<LetterContainer> capitalLetterContainers;
    [SerializeField] private List<LetterContainer> smallLetterContainers;

    private List<string> chosenLetters = new List<string>();
    private List<string> allLetters = new List<string>() { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

    private void Start()
    {
        PickRandomLetters();
        FillContainers();
    }

    private void PickRandomLetters()
    {
        List<string> tempLetters = new List<string>();

        for (int i = 0; i < 3; i++)
        {
            string tempLetter;

            do
            {
                tempLetter = allLetters[Random.Range(1, allLetters.Count)];
            } while (chosenLetters.Contains(tempLetter));

            chosenLetters.Add(tempLetter);
        }
    }

    private void FillContainers()
    {
        var randomIndexes = Shuffle(new List<int>() { 0, 1, 2 });

        for (int i = 0; i < capitalLetterContainers.Count; i++)
        {
            capitalLetterContainers[i].SetLetter(chosenLetters[randomIndexes[i]].ToUpper());
        }

        randomIndexes = Shuffle(new List<int>() { 0, 1, 2 });

        for (int i = 0; i < smallLetterContainers.Count; i++)
        {
            smallLetterContainers[i].SetLetter(chosenLetters[randomIndexes[i]].ToLower());
        }
    }

    private List<int> Shuffle(List<int> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n + 1);
            int value = list[k];
            list[k] = list[n];
            list[n] = value;
        }

        return (list);
    }
}