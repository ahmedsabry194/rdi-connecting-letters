﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LetterContainer : MonoBehaviour
{
    public string currentLetter;

    [SerializeField] private TextMeshProUGUI letter_TXT;

    private Animator animator;
    private LineRenderer line;
    private bool isPressed;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        line = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        if (isPressed)
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            line.SetPosition(1, mousePosition);
        }
    }

    public void SetLetter(string letter)
    {
        currentLetter = letter;
        letter_TXT.text = letter;
    }


    private void OnMouseDown()
    {
        isPressed = true;
        animator.SetBool("OnPress", true);
        GameManager.Instance.SetActiveLetter(this);

        line.SetPosition(0, transform.position);
    }

    private void OnMouseUp()
    {
        isPressed = false;
        animator.SetBool("OnPress", false);
        GameManager.Instance.SetActiveLetter(null);

        line.SetPosition(0, transform.position);
        line.SetPosition(1, transform.position);
    }

    private void OnMouseEnter()
    {
        if (GameManager.Instance.ActiveLetter == this || GameManager.Instance.ActiveLetter == null)
            return;

        if (GameManager.Instance.TargetLetter == this)
        {
            GameManager.Instance.OnCorrect();
        }
        else
        {
            print("wrong");
            isPressed = false;
            GameManager.Instance.SetActiveLetter(null);

            line.SetPosition(0, transform.position);
            line.SetPosition(1, transform.position);
        }
    }
}