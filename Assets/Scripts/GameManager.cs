﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public LetterContainer ActiveLetter { get; private set; }
    public LetterContainer TargetLetter { get; private set; }
    public int NumerOfCorrectAnswers { get; private set; }

    [SerializeField] private GameObject correctParticlePrefab;
    [SerializeField] private AiryUIAnimationManager winPanel;
    [SerializeField] private List<LetterContainer> capitalLetterContainers;
    [SerializeField] private List<LetterContainer> smallLetterContainers;

    public static GameManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SetActiveLetter(LetterContainer letterContainer)
    {
        if (letterContainer == null)
        {
            ActiveLetter = null;
            TargetLetter = null;

            return;
        }

        ActiveLetter = letterContainer;

        if (capitalLetterContainers.Contains(letterContainer))
        {
            TargetLetter = smallLetterContainers.Find(c => c.currentLetter.ToLower() == ActiveLetter.currentLetter.ToLower());

            return;
        }
        else if (smallLetterContainers.Contains(letterContainer))
        {
            TargetLetter = capitalLetterContainers.Find(c => c.currentLetter.ToLower() == ActiveLetter.currentLetter.ToLower());

            return;
        }
    }

    public void OnCorrect()
    {
        AudioManager.Instance.PlayAudio("correct");

        NumerOfCorrectAnswers++;
        Instantiate(correctParticlePrefab, ActiveLetter.transform.position, Quaternion.identity);
        Instantiate(correctParticlePrefab, TargetLetter.transform.position, Quaternion.identity);

        Destroy(ActiveLetter.gameObject);
        Destroy(TargetLetter.gameObject);

        if (NumerOfCorrectAnswers >= 3)
        {
            CoroutineHandler.Instance.PlayCoroutineWithTime(() =>
            {
                winPanel.ShowMenu();
                AudioManager.Instance.PlayAudio("clap");
            }, 2);
        }
    }

    public void OnWrong()
    {

    }

    public void RestartGame()
    {
        SceneManager.LoadScene("1 - Game");
    }
}